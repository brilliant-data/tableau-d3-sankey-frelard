# Sankey Diagram in Tableau Desktop using Frelard

![alt tag](https://raw.githubusercontent.com/brilliant-data/tableau-d3-sankey-demo/master/sankey.gif)

# Overview

This is the Desktop version of project https://github.com/kunzico/tableau-d3-sankey-demo using Frelard.

# Quickstart

1. Get Tableau Desktop PreRelease version 17.0814.2200 or above 
2. Copy the [Extensions](Extensions) folder to your Tableau Desktop repository folder ( Probably at: *C:\Users\\{user}\Documents\My Tableau Repository (Beta)* )
3. Install and run the server from root folder of the project:

```bash
npm install # Only for the first time
npm install gulp --global # Only for the first time
gulp watch serve # Running the server
```

4. Open Tableau Desktop and open workbook: [frelard_sankey_demo_with_extension.twbx](frelard_sankey_demo_with_extension.twbx)
5. The extension requires Full Data access, allow when prompted.






