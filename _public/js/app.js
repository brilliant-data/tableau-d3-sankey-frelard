var CANVAS_SELECTOR, TABLEAU_NULL, convertRowToObject, drawLinks, drawNodes, drawNodesAndLinks, drawSanKeyGraph, errorWrapped, getColumnIndexes, getTableau, initEditor, makeSanKeyData,
  slice = [].slice,
  indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

TABLEAU_NULL = '%null%';

getTableau = function() {
  return parent.parent.tableau;
};

errorWrapped = function(context, fn) {
  return function() {
    var args, err;
    args = 1 <= arguments.length ? slice.call(arguments, 0) : [];
    try {
      return fn.apply(null, args);
    } catch (error) {
      err = error;
      return console.error("Got error during '", context, "' : ", err.message, err.stack);
    }
  };
};

getColumnIndexes = function(table, required_keys) {
  var c, colIdxMaps, fn, j, len, ref;
  colIdxMaps = {};
  ref = table.columns;
  for (j = 0, len = ref.length; j < len; j++) {
    c = ref[j];
    fn = c.fieldName;
    if (indexOf.call(required_keys, fn) >= 0) {
      colIdxMaps[fn] = c.index;
    }
  }
  return colIdxMaps;
};

convertRowToObject = function(row, attrs_map) {
  return _.mapObject(attrs_map, function(id, name) {
    return row[id].value;
  });
};

CANVAS_SELECTOR = '#sankey-canvas';

makeSanKeyData = function(rowsIn, switchOnField, widthValue) {
  var all_names, cache, i, idx, j, k, len, len1, name, names, o, row, rows, value;
  rows = _.map(rowsIn, function(row) {
    return _.extend({}, row, (function() {
      switch (row[switchOnField]) {
        case "buyer":
          return {
            sellercod: "From: " + row.sellercod
          };
        case "seller":
          return {
            buyercod: "To: " + row.buyercod
          };
      }
    })());
  });
  all_names = _.pluck(rows, "sellercod").concat(_.pluck(rows, "buyercod"));
  i = 0;
  cache = {};
  for (j = 0, len = all_names.length; j < len; j++) {
    name = all_names[j];
    if (!cache.hasOwnProperty(name)) {
      cache[name] = i;
      i++;
    }
  }
  names = (function() {
    var results;
    results = [];
    for (name in cache) {
      idx = cache[name];
      results.push({
        name: "" + name,
        idx: idx
      });
    }
    return results;
  })();
  o = [];
  for (idx = k = 0, len1 = rows.length; k < len1; idx = ++k) {
    row = rows[idx];
    value = parseFloat(row[widthValue]);
    if (isNaN(value)) {
      value = 0.0;
    }
    o.push(_.extend({
      source: cache[row.sellercod],
      target: cache[row.buyercod],
      value: value
    }, row));
  }
  return {
    nodes: names,
    links: o
  };
};

drawNodes = function(svg, sankey, width, nodes) {
  var color, format, formatNumber, node;
  formatNumber = d3.format(",.0f");
  format = function(d) {
    return formatNumber(d) + " t";
  };
  color = d3.scaleOrdinal(d3.schemeCategory20);
  node = svg.append("g").selectAll(".node").data(nodes).enter().append("g").attr("class", "node").attr("transform", function(d) {
    return "translate(" + d.x + "," + d.y + ")";
  });
  node.append("rect").attr("height", function(d) {
    return Math.max(10, d.dy);
  }).attr("width", sankey.nodeWidth()).style("fill", function(d) {
    return d.color = color(d.name.replace(/ .*/, ""));
  }).style("stroke", function(d) {
    return d3.rgb(d.color).darker(2);
  }).append("title").text(function(d) {
    return d.name + "\n" + format(d.value);
  });
  node.append("text").attr("x", -6).attr("y", function(d) {
    return d.dy / 2;
  }).attr("dy", ".35em").attr("text-anchor", "end").attr("transform", null).text(function(d) {
    return d.name;
  }).filter(function(d) {
    return d.x < width / 2;
  }).attr("x", 6 + sankey.nodeWidth()).attr("text-anchor", "start");
  node.exit().remove();
  return node;
};

drawLinks = function(svg, sankey, path, links) {
  var format, formatNumber, link, linkTitle;
  formatNumber = d3.format(",.0f");
  format = function(d) {
    return formatNumber(d) + " t";
  };
  link = svg.append("g").selectAll(".link").data(links).enter().append("path").attr("class", "link").attr("d", path).style("stroke-width", function(d) {
    return Math.max(1, d.dy);
  }).sort(function(a, b) {
    return b.dy - a.dy;
  });
  link.exit().remove();
  linkTitle = link.append("title").text(function(d) {
    return d.source.name + " → " + d.target.name + "\n" + d.desc + "\n" + d.desig2 + "\n" + d.delyears + ": " + (format(d.value));
  });
  linkTitle.exit().remove();
  return link;
};

drawNodesAndLinks = function(svg, sankey, width, data) {
  var link, node, path;
  path = sankey.link();
  link = drawLinks(svg, sankey, path, data.links);
  return node = drawNodes(svg, sankey, width, data.nodes);
};

drawSanKeyGraph = function(data) {
  var bbox, height, margin, s, sankey, svg, width;
  svg = d3.select("#sankey-canvas");
  svg.selectAll("*").remove();
  if (data.links.length > 2500 || data.nodes.length > 512) {
    svg.node().setAttribute("height", "100px");
    console.error("TOO MUCH DATA: max 2500 links, 512 nodes");
    return;
  }
  margin = {
    top: 1,
    right: 1,
    bottom: 0,
    left: 1
  };
  width = 870;
  height = 830;
  svg.attr("width", width - margin.left - margin.right).attr("height", height - margin.top - margin.bottom).append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  sankey = d3.sankey().nodeWidth(15).nodePadding(10).size([width, height]);
  sankey.nodes(data.nodes).links(data.links).layout(1);
  drawNodesAndLinks(svg, sankey, width, data);
  s = svg.node();
  bbox = s.getBBox();
  s.setAttribute("viewBox", (bbox.x - 5) + " " + (bbox.y - 5) + " " + (bbox.width + 10) + " " + (bbox.height + 10));
  s.setAttribute("width", (bbox.width + 10) + "px");
  return s.setAttribute("height", (bbox.height + 10) + "px");
};

initEditor = function() {
  var tableau;
  tableau = getTableau();
  return tableau.extensions.initializeAsync().then(function() {
    var j, len, onDataLoadError, onDataLoadOk, results, sheet, updateEditor, worksheets;
    onDataLoadError = function(err) {
      return console.err("Error during Tableau Async request:", err);
    };
    onDataLoadOk = errorWrapped("Getting data from Tableau", function(table) {
      var col_indexes, data, row, sanKeyData;
      col_indexes = getColumnIndexes(table, table.columns.map(function(column) {
        return column.fieldName;
      }));
      data = (function() {
        var j, len, ref, results;
        ref = table.data;
        results = [];
        for (j = 0, len = ref.length; j < len; j++) {
          row = ref[j];
          results.push(convertRowToObject(row, col_indexes));
        }
        return results;
      })();
      sanKeyData = makeSanKeyData(data, "Pivot Field Names", "nrdel");
      return errorWrapped("Drawing SanKey diagram", drawSanKeyGraph)(sanKeyData);
    });
    updateEditor = function(changeEvent) {
      return changeEvent.worksheet.getUnderlyingDataAsync({
        maxRows: 0,
        ignoreSelection: false,
        includeAllColumns: true,
        ignoreAliases: true
      }).then(onDataLoadOk, onDataLoadError);
    };
    worksheets = tableau.extensions.dashboardContent.dashboard.worksheets;
    results = [];
    for (j = 0, len = worksheets.length; j < len; j++) {
      sheet = worksheets[j];
      results.push(sheet._eventListenerManagers["mark-selection-changed"].addEventListener(updateEditor));
    }
    return results;
  });
};

this.appApi = {
  initEditor: initEditor
};
